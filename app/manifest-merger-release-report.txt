-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-sdk
ADDED from AndroidManifest.xml:9:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:11:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:10:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:14:2
	android:name
		ADDED from AndroidManifest.xml:14:19
uses-permission#android.permission.RECEIVE_BOOT_COMPLETED
ADDED from AndroidManifest.xml:15:2
	android:name
		ADDED from AndroidManifest.xml:15:19
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:16:2
	android:name
		ADDED from AndroidManifest.xml:16:19
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:17:2
	android:name
		ADDED from AndroidManifest.xml:17:19
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:22
application
ADDED from AndroidManifest.xml:22:5
	android:label
		ADDED from AndroidManifest.xml:24:9
	android:allowBackup
		ADDED from AndroidManifest.xml:25:9
	android:icon
		ADDED from AndroidManifest.xml:23:9
	android:name
		ADDED from AndroidManifest.xml:26:9
activity#net.evoir.avenue225.SplashScreen
ADDED from AndroidManifest.xml:29:9
	android:label
		ADDED from AndroidManifest.xml:32:13
	android:theme
		ADDED from AndroidManifest.xml:31:13
	android:name
		ADDED from AndroidManifest.xml:30:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:33:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:34:17
	android:name
		ADDED from AndroidManifest.xml:34:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:36:17
	android:name
		ADDED from AndroidManifest.xml:36:27
activity#net.evoir.avenue225.MainActivity
ADDED from AndroidManifest.xml:41:9
	android:label
		ADDED from AndroidManifest.xml:43:13
	android:configChanges
		ADDED from AndroidManifest.xml:44:13
	android:name
		ADDED from AndroidManifest.xml:42:13
meta-data#android.app.searchable
ADDED from AndroidManifest.xml:46:13
	android:resource
		ADDED from AndroidManifest.xml:47:13
	android:name
		ADDED from AndroidManifest.xml:46:24
activity#net.evoir.avenue225.SearchResultsActivity
ADDED from AndroidManifest.xml:52:10
	android:name
		ADDED from AndroidManifest.xml:52:20
activity#net.evoir.avenue225.PostDetailActivity
ADDED from AndroidManifest.xml:58:18
	android:name
		ADDED from AndroidManifest.xml:60:13
activity#net.evoir.avenue225.SettingsActivity
ADDED from AndroidManifest.xml:65:9
	android:theme
		ADDED from AndroidManifest.xml:67:13
	android:name
		ADDED from AndroidManifest.xml:66:13
service#net.evoir.utils.JsonService
ADDED from AndroidManifest.xml:73:11
	android:name
		ADDED from AndroidManifest.xml:73:20
service#net.evoir.utils.NotificationService
ADDED from AndroidManifest.xml:74:11
	android:name
		ADDED from AndroidManifest.xml:74:20
receiver#net.evoir.utils.BootReceiver
ADDED from AndroidManifest.xml:76:5
	android:name
		ADDED from AndroidManifest.xml:76:15
intent-filter#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:77:9
action#android.intent.action.BOOT_COMPLETED
ADDED from AndroidManifest.xml:78:13
	android:name
		ADDED from AndroidManifest.xml:78:21
