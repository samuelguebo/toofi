package net.evoir.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Samuel Guebo on 2015/05/05, 01:49 PM, 04:49 PM.
 */
public class MobileReceiver extends BroadcastReceiver {

    private  Context mContext;
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        Toast.makeText(mContext,
                "MOBILE RECEIVER TRIGGERED",
                Toast.LENGTH_SHORT).show();
        startMobileReceiver();
    }
    private void startMobileReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.mobileManager();
    }
}
