package net.evoir.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
/**
 * Created by Colibri on 2015/05/05.
 */
public class BlueToothReceiver extends BroadcastReceiver {

        private Context mContext;
        @Override
        public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        startBluetoothReceiver();
        }

    private void startBluetoothReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.blueToothManager();
    }
}
