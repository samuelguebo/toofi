package net.evoir.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

/**
 * Created by Samuel Guebo on 2015/05/05, 01:49 PM, 04:49 PM.
 */
public class WifiReceiver extends BroadcastReceiver {

    private  Context mContext;
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        startWifiReceiver();
    }
    private void startWifiReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);

        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .isConnectedOrConnecting()) {
            netManager.mobileManager();

        }
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .isConnectedOrConnecting()) {
            netManager.mobileManager();

        }



    }
}
