package net.evoir.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import net.evoir.toofi.db.Model;
import net.evoir.toofi.notifications.BluetoothNotification;
import net.evoir.toofi.notifications.MobileNotification;
import net.evoir.toofi.notifications.WifiNotification;
import net.evoir.toofi.objects.Rezo;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

/**
 * Created by Samuel Guebo on 2015/05/05, 04:11 PM, 04:49 PM.
 */
public class NetWorkMultiManager {
    private Context mContext;
    private Rezo rezo;
    private Dao<Rezo,String> dao;
    public NetWorkMultiManager(Context context) {
        this.mContext = context;
    }
    public void wifiManager() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo =
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);



        if(networkInfo != null) {

                try {
                    dao = Model.getHelper(mContext).getDao(Rezo.class);

                    rezo = dao.queryForId("wifi");

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            Toast.makeText(mContext,
                    "SLUG IS: "+rezo.getSlug(),
                    Toast.LENGTH_SHORT).show();




            if(!networkInfo.isConnected()) {
                /*Toast.makeText(mContext,
                        "WIFI OFFLINE",
                        Toast.LENGTH_SHORT).show();*/

                    rezo.setStatus(0,mContext);


            }
            if(networkInfo.isConnected()) {




            }
            if(networkInfo.isConnectedOrConnecting()) {
            /*Toast.makeText(mContext,
                        "WIFI CONNECTED",
                        Toast.LENGTH_SHORT).show();*/
                WifiNotification notify = new WifiNotification(mContext);
                notify.sendNotification();
                rezo.setStatus(1, mContext);
            }

        }else {
            Toast.makeText(mContext,
                    "NO WIFI ADAPTER",
                    Toast.LENGTH_SHORT).show();
        }


        }
    public void mobileManager()
    {

            ConnectivityManager connectivityManager =
                    (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
            NetworkInfo mobileInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


            if (mobileInfo != null)  {
                try {
                    dao = Model.getHelper(mContext).getDao(Rezo.class);

                    rezo = dao.queryForId("mobile");

                    Toast.makeText(mContext,
                            "SLUG IS: "+rezo.getSlug(),
                            Toast.LENGTH_SHORT).show();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if(mobileInfo.isAvailable() && mobileInfo.getDetailedState()== NetworkInfo.DetailedState.CONNECTING){
                    Toast.makeText(mContext,
                            "SLUG IS: "+rezo.getSlug(),
                            Toast.LENGTH_SHORT).show();
                    MobileNotification notify = new MobileNotification(mContext);
                    notify.sendNotification();
                    rezo.setStatus(1, mContext);
                }
                if(!mobileInfo.isConnectedOrConnecting()){
                    rezo.setStatus(0, mContext);

                }

            }




    }

    public void blueToothManager() {
        BluetoothAdapter bluetoothAdapter;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            dao = Model.getHelper(mContext).getDao(Rezo.class);

            rezo = dao.queryForId("bluetooth");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(bluetoothAdapter.isEnabled()) {

            rezo.setStatus(1,mContext);
            BluetoothNotification notify = new BluetoothNotification(mContext);
            notify.sendNotification();

        } else {
            /*Toast.makeText(mContext,
                    "BLUETOOTH TURNED OFF",
                    Toast.LENGTH_SHORT).show();*/
            rezo.setStatus(0, mContext);

        }

    }

    public void GpsManager() {
        LocationManager lm = (LocationManager) mContext.getSystemService(mContext.LOCATION_SERVICE);
        boolean status = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (status) {
           /* Toast.makeText(mContext,
                    "GPS IS ON",
                    Toast.LENGTH_SHORT).show();*/
        }else {

            /*Toast.makeText(mContext,
                    "GPS IS OFF",
                    Toast.LENGTH_SHORT).show();*/
        }
    }

    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
    }
    public void stopMobileAdapter(boolean bool) {
        try { setMobileDataEnabled(mContext, bool); }
        catch (ClassNotFoundException e) {} catch (NoSuchFieldException e)  {} catch (IllegalAccessException e) {} catch (NoSuchMethodException e) {} catch (InvocationTargetException e) {}

    }
}

