package net.evoir.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

/**
 * Created by Colibri on 2015/05/05, 01:49 PM.
 */
public class GpsReceiver extends BroadcastReceiver {

    private Context mContext;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.mContext = context;

        startGpsReceiver();
    }
    private void startGpsReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.GpsManager();
    }
}
