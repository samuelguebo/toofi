package net.evoir.toofi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;


import net.evoir.toofi.adapters.RezoAdapter;
import net.evoir.toofi.db.Model;
import net.evoir.toofi.objects.Rezo;
import net.evoir.utils.BlueToothReceiver;
import net.evoir.utils.NetWorkMultiManager;

import org.apache.http.impl.conn.tsccm.RefQueueWorker;

import java.sql.SQLException;
import java.util.List;

public class MainActivity extends Activity {
    private ListView listView ;
    private Context mContext;
    private List<Rezo> networkList;
    private Dao<Rezo, String> dao;
    private BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mContext = this;
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.networkList);
        networkList = getNetworkList();

        // Start all Network Receivers
        startBluetoothReceiver();
        startWifiReceiver();
        startGpsReceiver();

        // Assign adapter to ListView
        if (!networkList.isEmpty()) {
            RezoAdapter adapter = new RezoAdapter(this,networkList);
            listView.setAdapter(adapter);

            // ListView Item Click Listener
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //
                }
            });

        }

    // set background color of Action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_black));

    }

    private void startBluetoothReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.blueToothManager();
    }
    private void startWifiReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.wifiManager();
    }
    private void startGpsReceiver() {
        NetWorkMultiManager netManager = new NetWorkMultiManager(mContext);
        netManager.GpsManager();
    }
    //set Listener for ListView
    private List<Rezo> getNetworkList() {
        List <Rezo> finalList = null;

        try {
            dao = Model.getHelper(mContext).getDao(Rezo.class);
            finalList = dao.queryForAll();

            /*Toast.makeText(mContext,
                    "SLUGS ARE : " +finalList.size(),
                    Toast.LENGTH_SHORT).show();*/

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(finalList.isEmpty()){
            finalList.add(new Rezo("Wifi", "wifi", R.drawable.ic_wifi,"0",0)); // Add wifi
            finalList.add(new Rezo("Bluetooth", "bluetooth",R.drawable.ic_bluetooth ,"0",0)); // Add BlueTooth
            finalList.add(new Rezo("Mobile", "mobile", R.drawable.ic_mobile,"0",0)); // Add Data
            finalList.add(new Rezo("GPS", "gps", R.drawable.ic_gps,"0",0)); // Add GPS

            for(int i=0;i<finalList.size();i++) {
                Rezo temp;
                temp = finalList.get(i);
                try {
                    Model.getHelper(mContext).getDao(Rezo.class).createOrUpdate(temp);
                } catch (SQLException er) {
                    // TODO Auto-generated catch block
                    er.printStackTrace();
                    Log.v("mytag", " Dao error" + er.getMessage());

                }
            }
        }
        return finalList;
    }

    public void loadDefaultSettings() {
        PreferenceManager.setDefaultValues(mContext, R.xml.settings_fragment_bluetooth, false);
        PreferenceManager.setDefaultValues(mContext, R.xml.settings_fragment_mobile, false);
        PreferenceManager.setDefaultValues(mContext, R.xml.settings_fragment_wifi, false);
        PreferenceManager.setDefaultValues(mContext, R.xml.settings_fragment_gps, false);
    }

}