package net.evoir.toofi.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import net.evoir.toofi.db.Model;
import net.evoir.toofi.notifications.StopMobile;
import net.evoir.toofi.notifications.StopWifi;
import net.evoir.toofi.objects.Rezo;
import net.evoir.utils.Constants;
import net.evoir.utils.NetWorkMultiManager;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

/**
 * Created by Samuel Guebo on 2015/05/07, 07:03 PM.
 */
public class MobileNotificationService extends Service {
    private Context mContext;
    private PowerManager.WakeLock mWakeLock;
    private Rezo rezo;
    private Dao <Rezo,String> dao;
    private String TAG ="3G";

    /**
     * Simply return null, since our Service will not be communicating with
     * any other components. It just does its work silently.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * This is where we initialize. We call this when onStart/onStartCommand is
     * called by the system. We won't do anything with the intent here, and you
     * probably won't, either.
     */
    private void handleIntent(Intent intent) {

        // obtain the wake lock
        mContext = getApplicationContext();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Constants.TAG);
        mWakeLock.acquire();

        try {
            dao = Model.getHelper(mContext).getDao(Rezo.class);
            rezo = dao.queryForId("mobile");


        } catch (SQLException e) {
            e.printStackTrace();
        }

        startNotification();
        //start counting to 15, if Wifi is still on, and user did not answer, shut it down
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // shutdown automatically if user does not answer
                checkUserAnswer();
            }
        }, 15000);


    }

    private void checkUserAnswer() {

        if (rezo.getStatus()<2) {
            // Stop Mobile
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo mobileInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            boolean mobileEnabled = mobileInfo.isConnectedOrConnecting();
            if(mobileEnabled){
                NetWorkMultiManager manager = new NetWorkMultiManager(mContext);
                manager.stopMobileAdapter(false);
                rezo.setStatus(0, mContext);


            }

        }
    }


    /**
     * This is deprecated, but you have to implement it if you're planning on
     * supporting devices with an API level lower than 5 (Android 2.0).
     */
    @Override
    public void onStart(Intent intent, int startId) {
        handleIntent(intent);
    }

    /**
     * This is called on 2.0+ (API level 5 or higher). Returning
     * START_NOT_STICKY tells the system to not restart the service if it is
     * killed because of poor resource (memory/cpu) conditions.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return START_NOT_STICKY;
    }

    /**
     * In onDestroy() we release our wake lock. This ensures that whenever the
     * Service stops (killed for resources, stopSelf() called, etc.), the wake
     * lock will be released.
     */
    public void onDestroy() {
        super.onDestroy();
        mWakeLock.release();
    }

    /**
     * Perfome desired task such as get New post number
     */


public void startNotification() {
        SharedPreferences prefs;
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);


        int notifications_type_mobile  =Integer.parseInt(prefs.getString("typeNotificationArray", "1")) ;
        int notifications_interval_mobile  =Integer.parseInt(prefs.getString("intervalArray_mobile", "1")) ;
        Boolean activate_notification = prefs.getBoolean("activate_notifications_mobile", true);
        Toast.makeText(mContext,
                "BOOLEAN IS " + activate_notification,
                Toast.LENGTH_SHORT).show();

        if (activate_notification) {

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(mContext)
                            .setSmallIcon(rezo.getImage())
                            .setContentTitle("Alerte " + TAG)
                                    //.setContentText("Le Wifi est activé depuis "+notifications_interval_wifi+" minutes");
                            .setContentText("La "+TAG+" est activée depuis " + notifications_interval_mobile + " minutes");
            int mNotificationId = 003;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            //prepare the pending Intent
            Intent resultIntent = new Intent(mContext, StopMobile.class);
            // Because clicking the notification opens a new ("special") activity, there's
            // no need to create an artificial back stack.
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            mContext,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setAutoCancel(true);
            mBuilder.setLights(Color.MAGENTA, 1000, 1000);

            //get data from SharedPreferences

            /*boolean vibrate = prefs.getBoolean("activate_vibrate", false);
            if(vibrate){
            */
            long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
            mBuilder.setVibrate(pattern);
            /*}*/

            /*String ringtone = prefs.getString("ringtone", "default ringtone");
            if (ringtone!="default ringtone"){*/
            //Uri ringToneUri = Uri.parse(ringtone);
            if (notifications_type_mobile > 1)   {
                Uri ringToneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext.getApplicationContext(),
                        RingtoneManager.TYPE_RINGTONE);
                // Uri ringToneUri = Uri.parse("default ringtone");
                mBuilder.setSound(ringToneUri);
            /*}*/
            }
            mBuilder.setContentIntent(resultPendingIntent);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
        }

    }
}
