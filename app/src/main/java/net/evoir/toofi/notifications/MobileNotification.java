package net.evoir.toofi.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import net.evoir.toofi.services.MobileNotificationService;
import net.evoir.toofi.services.WifiNotificationService;

/**
 * Created by Samuel Guebo on 2015/05/07, 03:53 PM, 04:48 PM.
 */

public class MobileNotification {
    private SharedPreferences prefs;
    private Context mContext;

    public MobileNotification(Context context) {
        mContext = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void sendNotification() {

        Toast.makeText(mContext,
                "MOBILE: NOTIFICATION SCHEDULED",
                Toast.LENGTH_SHORT).show();
        // Restart service every 30 seconds
//        int notifications_interval_wifi  =5;
        int notifications_interval_mobile  =Integer.parseInt(prefs.getString("intervalArray_mobile", "1")) ;


        //final long REPEAT_TIME = 1000 * notifications_interval_wifi;
        final long DELAY = notifications_interval_mobile;

        Toast.makeText(mContext,
                "MOBILE INTERVAL IS :"+DELAY,
                Toast.LENGTH_SHORT).show();

        AlarmManager am=(AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, MobileNotificationService.class);
        PendingIntent pi = PendingIntent.getService(mContext, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (1000 *60 * DELAY), pi);
//        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (1000 * 60 * 5), pi);


    }
}
