package net.evoir.toofi.notifications;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import net.evoir.toofi.R;
import net.evoir.toofi.db.Model;
import net.evoir.toofi.objects.Rezo;

import java.sql.SQLException;

/**
 * Created by Colibri on 2015/05/07, 04:22 PM.
 */
public class StopWifi extends Activity {
    private Context mContext;
    private Dao<Rezo, String> dao;
    private Rezo rezo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        this.mContext = this;

    }
    @Override
    protected void onStart() {
        super.onStart();
        loadDialog();
    }

    private void loadDialog() {
        // Ask a validation
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Stop Wifi?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    // handle answers
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            try {
                dao = Model.getHelper(mContext).getDao(Rezo.class);
                rezo = dao.queryForId("wifi");

            } catch (SQLException e) {
                e.printStackTrace();
            }
            WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            boolean wifiEnabled = wifiManager.isWifiEnabled();
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    // Stop Wifi
                    wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
                    wifiEnabled = wifiManager.isWifiEnabled();

                    if(wifiEnabled){
                        wifiManager.setWifiEnabled(false);
                        rezo.setStatus(0, mContext);
                        Toast.makeText(mContext,
                                "WIFI SWITCHED OFF",
                                Toast.LENGTH_SHORT).show();



                    }
                    finish();


                    break;

                case DialogInterface.BUTTON_NEGATIVE:

                    // Leave Wifi running
                    rezo.setStatus(2,mContext);

                    // restart Wifi if stopped automatically
                    wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
                    wifiEnabled = wifiManager.isWifiEnabled();
                    if(!wifiEnabled){
                        wifiManager.setWifiEnabled(true);
                        Toast.makeText(mContext,
                                "WIFI SWITCHED ON",
                                Toast.LENGTH_SHORT).show();

                        //send broadcast receiver
                        Intent intent = new Intent();
                        intent.setAction("android.net.wifi.WIFI_STATE_CHANGED");
                        sendBroadcast(intent);

                    }
                    finish();
                    break;
            }
        }
    };
}