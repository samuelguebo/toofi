package net.evoir.toofi.notifications;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import net.evoir.toofi.R;
import net.evoir.toofi.services.WifiNotificationService;

import java.util.Calendar;

/**
 * Created by Samuel Guebo on 2015/05/07, 03:53 PM, 04:48 PM.
 */

public class WifiNotification {
    private SharedPreferences prefs;
    private Context mContext;

    public WifiNotification(Context context) {
        mContext = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void sendNotification() {

        // Restart service every 30 seconds
//        int notifications_interval_wifi  =5;
        int notifications_interval_wifi  =Integer.parseInt(prefs.getString("intervalArray_wifi", "1")) ;
        //final long REPEAT_TIME = 1000 * notifications_interval_wifi;
        final long DELAY = notifications_interval_wifi;


        AlarmManager am=(AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, WifiNotificationService.class);
        PendingIntent pi = PendingIntent.getService(mContext, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (1000 *60 * DELAY), pi);
//        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (1000 * 60 * 5), pi);


    }
}
