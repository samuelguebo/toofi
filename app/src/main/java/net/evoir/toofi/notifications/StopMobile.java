package net.evoir.toofi.notifications;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import net.evoir.toofi.R;
import net.evoir.toofi.db.Model;
import net.evoir.toofi.objects.Rezo;
import net.evoir.utils.NetWorkMultiManager;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

/**
 * Created by Colibri on 2015/05/07, 04:22 PM.
 */
public class StopMobile extends Activity {
    private Context mContext;
    private Dao<Rezo, String> dao;
    private Rezo rezo;
    private String TAG = "3G";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        this.mContext = this;

    }
    @Override
    protected void onStart() {
        super.onStart();
        loadDialog();
    }

    private void loadDialog() {
        // Ask a validation
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Stop "+TAG+"?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    // handle answers
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            try {
                dao = Model.getHelper(mContext).getDao(Rezo.class);
                rezo = dao.queryForId("mobile");

            } catch (SQLException e) {
                e.printStackTrace();
            }
            // Stop Mobile
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo mobileInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            boolean mobileEnabled = mobileInfo.isConnectedOrConnecting();

            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    // Stop Mobile

                    if(mobileEnabled){
                        NetWorkMultiManager manager = new NetWorkMultiManager(mContext);
                        manager.stopMobileAdapter(true);
                        rezo.setStatus(0, mContext);
                        Toast.makeText(mContext,
                                TAG+" SWITCHED OFF",
                                Toast.LENGTH_SHORT).show();



                    }
                    finish();


                    break;

                case DialogInterface.BUTTON_NEGATIVE:

                    // Leave Mobile running or restart it if closed
                    rezo.setStatus(2,mContext);
                    if(!mobileEnabled){
                        NetWorkMultiManager manager = new NetWorkMultiManager(mContext);
                        manager.stopMobileAdapter(true);
                        rezo.setStatus(0, mContext);
                        Toast.makeText(mContext,
                                TAG+" SWITCHED OFF",
                                Toast.LENGTH_SHORT).show();


                    //send broadcast receiver
                    Intent intent = new Intent();
                    intent.setAction("android.net.conn.CONNECTIVITY_CHANGE");
                    sendBroadcast(intent);
                    }
                    finish();
                    break;
            }
        }


    };
}