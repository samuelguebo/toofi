package net.evoir.toofi.notifications;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import net.evoir.toofi.R;
import net.evoir.toofi.db.Model;
import net.evoir.toofi.objects.Rezo;

import java.sql.SQLException;

/**
 * Created by Colibri on 2015/05/07, 04:22 PM.
 */
public class StopBluetooth extends Activity {
    private Context mContext;
    private Dao<Rezo, String> dao;
    private Rezo rezo;
    private String TAG = "Bluethooth";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        this.mContext = this;

    }
    @Override
    protected void onStart() {
        super.onStart();
        loadDialog();
    }

    private void loadDialog() {
        // Ask a validation
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Stop "+TAG+"?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    // handle answers
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            try {
                dao = Model.getHelper(mContext).getDao(Rezo.class);
                rezo = dao.queryForId("bluetooth");

            } catch (SQLException e) {
                e.printStackTrace();
            }
            BluetoothAdapter bluetoothManager = BluetoothAdapter.getDefaultAdapter();
            boolean bluetoothEnabled = bluetoothManager.isEnabled();
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    // Stop Wifi
                    if(bluetoothEnabled){
                        bluetoothManager.disable();
                        rezo.setStatus(0, mContext);
                        Toast.makeText(mContext,
                                TAG+" SWITCHED OFF",
                                Toast.LENGTH_SHORT).show();



                    }
                    finish();


                    break;

                case DialogInterface.BUTTON_NEGATIVE:

                    // Leave Wifi running
                    rezo.setStatus(2,mContext);

                    // restart Wifi if stopped automatically
                    if(!bluetoothEnabled){
                        bluetoothManager.enable();
                        Toast.makeText(mContext,
                                TAG+" SWITCHED ON",
                                Toast.LENGTH_SHORT).show();


                        //send broadcast receiver
                        Intent intent = new Intent();
                        intent.setAction("android.bluetooth.adapter.action.STATE_CHANGED");
                        sendBroadcast(intent);
                    }
                    finish();
                    break;
            }
        }
    };
}