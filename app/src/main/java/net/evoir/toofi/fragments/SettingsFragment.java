package net.evoir.toofi.fragments;


import net.evoir.toofi.R;
import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {
	   
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    
	   super.onCreate(savedInstanceState);
          if(getArguments() != null) {
              String slug = getArguments().getString("slug");
              if ("bluetooth".equals(slug)) {
                  addPreferencesFromResource(R.xml.settings_fragment_bluetooth);

              } else if ("mobile".equals(slug)) {
                  addPreferencesFromResource(R.xml.settings_fragment_mobile);

              } else if ("gps".equals(slug)) {
                  addPreferencesFromResource(R.xml.settings_fragment_gps);

              }else {
                  addPreferencesFromResource(R.xml.settings_fragment_wifi);

              }
          }else {
              addPreferencesFromResource(R.xml.settings_fragment_wifi);
          }
 }

}